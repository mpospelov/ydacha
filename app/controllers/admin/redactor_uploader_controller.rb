class Admin::RedactorUploaderController < ApplicationController

  def index
    file = Assets::Redactor.create!(data: params[:file])
    render json: {filelink: file.data.url}
  end

  def loaded
    @images = Assets::Redactor.where("created_at > ?", 1.day.ago).map{|ri| {thumb: ri.data.url, image: ri.data.url} }
    render json: @images
  end

end
