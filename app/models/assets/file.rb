class Assets::File < ActiveRecord::Base
  has_attached_file :data, default_url: "/system/#{self.to_s.downcase.gsub("::","/").pluralize}/:attachment/:style/missing.png"

  belongs_to :fileable, polymorphic: true
end
