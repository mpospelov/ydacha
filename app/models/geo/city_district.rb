class Geo::CityDistrict < ActiveRecord::Base

  init_rails_admin_localization

  has_and_belongs_to_many :real_estate_products, class_name: "RealEstate::Product"

  def self.admin_visible_columns
    [:name]
  end

  def self.admin_visible_fields
    [:name]
  end

end
