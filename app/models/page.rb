class Page < ActiveRecord::Base
  init_rails_admin_localization

  def self.admin_visible_columns
    [:title, :permalink]
  end

  def self.admin_visible_fields
    {
      permalink: :string,
      title: :string,
      body: :wysihtml5,
      visible: :boolean
    }
  end

end
