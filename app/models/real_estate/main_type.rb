class RealEstate::MainType < RealEstate::Type

  init_rails_admin_localization

  has_and_belongs_to_many :products, class_name: "RealEstate::Product"

  has_many :root_products, class_name: "RealEstate::Product"

end
