class RealEstate::Product < ActiveRecord::Base

  init_rails_admin_localization do |rails_admin|
    rails_admin.list do
      filters [:title]
    end
  end

  belongs_to :manager, class_name: "Users::Manager"
  belongs_to :owner, class_name: "Users::Visitor"

  has_and_belongs_to_many :types, class_name: "RealEstate::MainType", association_foreign_key: :type_id
  has_and_belongs_to_many :land_types, class_name: "RealEstate::LandType", association_foreign_key: :type_id
  has_and_belongs_to_many :permission_types, class_name: "RealEstate::PermissionType", association_foreign_key: :type_id
  has_and_belongs_to_many :city_districts, class_name: "Geo::CityDistrict"
  has_and_belongs_to_many :directions, class_name: "Geo::Direction"

  belongs_to :root_type, class_name: "RealEstate::MainType"
  belongs_to :sub_type, class_name: "RealEstate::SubType"

  #has_one :subtype

  has_many :bids

  delegate :email, :phone, :name, to: :owner

  validates_presence_of :manager_id, :state, :root_type, :owner_id

  def manager_name
    manager.name
  end

  def bids_count
    bids.size
  end

  def type_name
    root_type.name
  end

  class << self

    def available_states
      ["Не опубликовано", "Опубликовано", "Удалено"]
    end

    def admin_visible_fields
      {
        id: :string, manager: :partial, state: :partial, root_type: :partial,
        types: :partial, sub_type: :partial, title: :string, city_districts: :partial,
        directions: :partial, operation: :partial, description: :wysihtml5, owner: :partial, show_map: :boolean,
        show_city: :boolean, mcad_distance: :string, price: :partial, commission: :string,
        old_price: :string, near_city: :string, closest_railway: :string, floors_count: :string,
        building_square: :string, land_square: :string, land_types: :partial, permission_types: :partial, heat: :boolean,
        gas: :boolean, energy: :boolean, phone_line: :boolean
      }
    end

    def admin_visible_columns
      [:manager_name, :type_name, :title, :price, :state, :bids_count]
    end

  end

end
