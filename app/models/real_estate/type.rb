class RealEstate::Type < ActiveRecord::Base
  init_rails_admin_localization{ |rails_admin| rails_admin.visible false}

  has_and_belongs_to_many :products

  def self.admin_visible_fields
    [:name]
  end

  def self.admin_visible_columns
    [:name]
  end

end
