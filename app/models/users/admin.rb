class Users::Admin < Users::Base

  init_rails_admin_localization

  def admin?
    true
  end

end
