class Users::Base < ActiveRecord::Base

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  validates_presence_of :email
  validates_presence_of :password, :password_confirmation, on: :create
  validates :email, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, message: "Неправильный адрес почты" }

  has_one :photo, as: :fileable, class_name: "Assets::Image", dependent: :destroy

  accepts_nested_attributes_for :photo

  def method_missing(meth, *args, &block)
    if meth.to_s =~ /^(admin|manager|visitor)\?$/
      false
    else
      super
    end
  end

end

