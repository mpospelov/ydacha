class Users::Manager < Users::Base

  init_rails_admin_localization

  def manager?
    true
  end

  def self.admin_visible_fields
    { name: :string, email: :string, password: :password, password_confirmation: :password, photo: :partial }
  end

  def self.admin_visible_columns
    [:name, :email]
  end

end
