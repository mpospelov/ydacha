class Users::Visitor < Users::Base

  init_rails_admin_localization

  has_many :products, class_name: "RealEstate::Product"

  def visitor?
    true
  end

  def self.admin_visible_fields
    [:email, :name, :phone, :password, :password_confirmation]
  end

  def self.admin_visible_columns
    [:email, :last_sign_in_at, :last_sign_in_ip, :created_at]
  end

end
