ActiveRecord::Base.class_eval do
  def self.init_rails_admin_localization
    rails_admin do
      class_name = self.abstract_model.model_name
      set_labels class_name
      set_table_view class_name
      set_form_view class_name
      yield(self) if block_given?
    end
  end
end

RailsAdmin::Config::Sections::Edit.class_eval do

  def parse_form_fields_hash fields, class_name
    form_class_name = class_name.underscore
    fields.each do |field_name, field_type|
      if field_type == :partial
        field field_name do
          label I18n.t(field_name, scope: "attributes.#{form_class_name}", default: field_name)
          partial "#{form_class_name}/#{field_name}"
        end
      else
        field field_name, field_type do
          label I18n.t(field_name, scope: "attributes.#{form_class_name}", default: field_name)
        end
      end
    end
  end

end

RailsAdmin::Config::Model.class_eval do

  def set_form_view class_name
    fields = class_name.constantize.try(:admin_visible_fields) || class_name.constantize.column_names
    edit do
      parse_form_fields_hash fields, class_name
    end
  end

  def set_table_view class_name
    table_class_name = class_name.underscore
    columns = class_name.constantize.try(:admin_visible_columns) || class_name.constantize.column_names
    list do
      columns.each do |column_name|
        field column_name do
          label I18n.t(column_name, scope: "attributes.#{table_class_name}", default: column_name)
        end
      end
    end
  end

  def set_labels class_name
    label_class_name = class_name.underscore
    label{I18n.t "one", scope: "models.#{label_class_name}"}
    label_plural{I18n.t "other", scope: "models.#{label_class_name}"}
  end

end
