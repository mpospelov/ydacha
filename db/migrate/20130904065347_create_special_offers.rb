class CreateSpecialOffers < ActiveRecord::Migration
  def change
    create_table :special_offers do |t|
      t.integer :position

      t.timestamps
    end
  end
end
