class CreateBids < ActiveRecord::Migration
  def change
    create_table :bids do |t|
      t.string :first_name
      t.string :second_name
      t.text :body

      t.timestamps
    end
  end
end
