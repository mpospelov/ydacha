class CreateGeoDirections < ActiveRecord::Migration
  def change
    create_table :geo_directions do |t|
      t.string :name

      t.timestamps
    end
  end
end
