class CreateGeoRegions < ActiveRecord::Migration
  def change
    create_table :geo_regions do |t|
      t.string :name

      t.timestamps
    end
  end
end
