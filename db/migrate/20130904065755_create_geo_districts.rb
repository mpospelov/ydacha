class CreateGeoDistricts < ActiveRecord::Migration
  def change
    create_table :geo_districts do |t|
      t.integer :region_id
      t.string :name

      t.timestamps
    end
  end
end
