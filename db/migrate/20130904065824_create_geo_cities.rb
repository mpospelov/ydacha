class CreateGeoCities < ActiveRecord::Migration
  def change
    create_table :geo_cities do |t|
      t.integer :region_id
      t.string :name

      t.timestamps
    end
  end
end
