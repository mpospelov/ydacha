class CreateGeoCityDistricts < ActiveRecord::Migration
  def change
    create_table :geo_city_districts do |t|
      t.integer :city_id
      t.string :name

      t.timestamps
    end
  end
end
