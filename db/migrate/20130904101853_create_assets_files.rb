class CreateAssetsFiles < ActiveRecord::Migration
  def change
    create_table :assets_files do |t|
      t.string :type

      t.timestamps
    end
  end
end
