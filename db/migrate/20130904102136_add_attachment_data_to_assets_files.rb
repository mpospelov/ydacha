class AddAttachmentDataToAssetsFiles < ActiveRecord::Migration
  def self.up
    change_table :assets_files do |t|
      t.attachment :data
    end
  end

  def self.down
    drop_attached_file :assets_files, :data
  end
end
