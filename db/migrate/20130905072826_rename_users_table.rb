class RenameUsersTable < ActiveRecord::Migration
  def change
    rename_table :users, :users_bases
  end
end
