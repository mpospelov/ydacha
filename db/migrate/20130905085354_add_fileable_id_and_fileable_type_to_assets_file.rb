class AddFileableIdAndFileableTypeToAssetsFile < ActiveRecord::Migration
  def change
    add_column :assets_files, :fileable_id, :integer
    add_column :assets_files, :fileable_type, :string
  end
end
