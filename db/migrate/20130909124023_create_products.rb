class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.integer :manager_id
      t.string :state
      t.integer :type_id
      t.integer :subtype_id
      t.string :title
      t.string :operation
      t.string :visitor_id
      t.boolean :show_map
      t.boolean :show_city
      t.string :mcad_distance
      t.string :price
      t.string :currency
      t.string :commission
      t.string :old_price
      t.string :near_city
      t.string :closest_railway
      t.string :floors_count
      t.string :building_square
      t.string :land_square
      t.string :land_type
      t.string :permission
      t.boolean :heat
      t.boolean :gas
      t.boolean :energy
      t.boolean :phone_line

      t.timestamps
    end
  end
end
