class AddViewsCountToProducts < ActiveRecord::Migration
  def change
    add_column :products, :views_count, :integer
  end
end
