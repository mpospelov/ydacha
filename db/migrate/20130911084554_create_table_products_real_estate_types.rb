class CreateTableProductsRealEstateTypes < ActiveRecord::Migration
  def change
    create_table :products_real_estate_types do |t|
      t.integer :product_id
      t.integer :real_estate_type_id
    end
    add_index :products_real_estate_types, [:product_id, :real_estate_type_id], unique: true, name: 'index_prods_vs_retypes'
  end
end
