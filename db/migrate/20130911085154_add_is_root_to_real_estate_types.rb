class AddIsRootToRealEstateTypes < ActiveRecord::Migration
  def change
    add_column :real_estate_types, :is_root, :boolean
  end
end
