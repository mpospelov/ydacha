class AddRealEstateModuleToProductsAndTypes < ActiveRecord::Migration
  def change
    rename_table :products, :real_estate_products
    rename_table :products_real_estate_types, :real_estate_products_types
    rename_column :real_estate_products_types, :real_estate_type_id, :type_id
  end
end
