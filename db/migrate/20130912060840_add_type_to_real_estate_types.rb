class AddTypeToRealEstateTypes < ActiveRecord::Migration
  def change
    add_column :real_estate_types, :type, :string
  end
end
