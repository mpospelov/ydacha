class CreateGeoCityDistrictsRealEstateProducts < ActiveRecord::Migration
  def change
    create_table :geo_city_districts_real_estate_products do |t|
      t.integer :city_id
      t.integer :product_id
    end
    add_index :geo_city_districts_real_estate_products, [:city_id, :product_id], unique: true, name: "index_city_dists_re_prods"
  end
end
