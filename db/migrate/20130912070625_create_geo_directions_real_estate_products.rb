class CreateGeoDirectionsRealEstateProducts < ActiveRecord::Migration
  def change
    create_table :geo_directions_real_estate_products do |t|
      t.integer :direction_id
      t.integer :product_id
    end
    add_index :geo_directions_real_estate_products, [:direction_id, :product_id], unique: true, name: "index_geo_direcs_re_prods"
  end
end
