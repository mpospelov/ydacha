class AddDescriptionToRealEstateProducts < ActiveRecord::Migration
  def change
    add_column :real_estate_products, :description, :text
  end
end
