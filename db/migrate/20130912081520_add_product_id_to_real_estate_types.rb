class AddProductIdToRealEstateTypes < ActiveRecord::Migration
  def change
    add_column :real_estate_types, :product_id, :integer
  end
end
