class AddRootTypeIdToRealEstateProducts < ActiveRecord::Migration
  def change
    add_column :real_estate_products, :root_type_id, :integer
  end
end
