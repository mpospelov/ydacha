class AddSubTypeIdToRealEstateProducts < ActiveRecord::Migration
  def change
    add_column :real_estate_products, :sub_type_id, :integer
  end
end
