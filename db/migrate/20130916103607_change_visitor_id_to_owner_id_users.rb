class ChangeVisitorIdToOwnerIdUsers < ActiveRecord::Migration
  def change
    rename_column :real_estate_products, :visitor_id, :owner_id
  end
end
