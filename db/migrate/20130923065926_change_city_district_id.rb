class ChangeCityDistrictId < ActiveRecord::Migration
  def change
    rename_column :geo_city_districts_real_estate_products, :city_id, :city_district_id
  end
end
