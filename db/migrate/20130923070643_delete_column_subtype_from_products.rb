class DeleteColumnSubtypeFromProducts < ActiveRecord::Migration
  def change
    remove_column :real_estate_products, :subtype_id
  end
end
