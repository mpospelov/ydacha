# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20130923070643) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "assets_files", force: true do |t|
    t.string   "type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "data_file_name"
    t.string   "data_content_type"
    t.integer  "data_file_size"
    t.datetime "data_updated_at"
    t.integer  "fileable_id"
    t.string   "fileable_type"
  end

  create_table "bids", force: true do |t|
    t.string   "first_name"
    t.string   "second_name"
    t.text     "body"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "product_id"
  end

  create_table "geo_cities", force: true do |t|
    t.integer  "region_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geo_city_districts", force: true do |t|
    t.integer  "city_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geo_city_districts_real_estate_products", force: true do |t|
    t.integer "city_district_id"
    t.integer "product_id"
  end

  add_index "geo_city_districts_real_estate_products", ["city_district_id", "product_id"], name: "index_city_dists_re_prods", unique: true, using: :btree

  create_table "geo_directions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geo_directions_real_estate_products", force: true do |t|
    t.integer "direction_id"
    t.integer "product_id"
  end

  add_index "geo_directions_real_estate_products", ["direction_id", "product_id"], name: "index_geo_direcs_re_prods", unique: true, using: :btree

  create_table "geo_districts", force: true do |t|
    t.integer  "region_id"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "geo_regions", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "pages", force: true do |t|
    t.string   "permalink"
    t.string   "title"
    t.text     "body"
    t.boolean  "visible"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "rails_admin_histories", force: true do |t|
    t.text     "message"
    t.string   "username"
    t.integer  "item"
    t.string   "table"
    t.integer  "month",      limit: 2
    t.integer  "year",       limit: 8
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "rails_admin_histories", ["item", "table", "month", "year"], name: "index_rails_admin_histories", using: :btree

  create_table "real_estate_products", force: true do |t|
    t.integer  "manager_id"
    t.string   "state"
    t.integer  "type_id"
    t.string   "title"
    t.string   "operation"
    t.string   "owner_id"
    t.boolean  "show_map"
    t.boolean  "show_city"
    t.string   "mcad_distance"
    t.string   "price"
    t.string   "currency"
    t.string   "commission"
    t.string   "old_price"
    t.string   "near_city"
    t.string   "closest_railway"
    t.string   "floors_count"
    t.string   "building_square"
    t.string   "land_square"
    t.string   "land_type"
    t.string   "permission"
    t.boolean  "heat"
    t.boolean  "gas"
    t.boolean  "energy"
    t.boolean  "phone_line"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "views_count"
    t.text     "description"
    t.integer  "root_type_id"
    t.integer  "sub_type_id"
  end

  create_table "real_estate_products_types", force: true do |t|
    t.integer "product_id"
    t.integer "type_id"
  end

  add_index "real_estate_products_types", ["product_id", "type_id"], name: "index_prods_vs_retypes", unique: true, using: :btree

  create_table "real_estate_types", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "is_root"
    t.string   "type"
    t.integer  "product_id"
  end

  create_table "special_offers", force: true do |t|
    t.integer  "position"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users_bases", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "type"
    t.string   "name"
    t.string   "phone"
  end

  add_index "users_bases", ["email"], name: "index_users_bases_on_email", unique: true, using: :btree
  add_index "users_bases", ["reset_password_token"], name: "index_users_bases_on_reset_password_token", unique: true, using: :btree

end
